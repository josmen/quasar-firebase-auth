// import { boot } from 'quasar/wrappers'

// // "async" is optional;
// // more info on params: https://v2.quasar.dev/quasar-cli/boot-files
// export default boot(async (/* { app, router, ... } */) => {
//   // something to do
// })

import { initializeApp } from 'firebase/app'
import { getAuth, onAuthStateChanged } from 'firebase/auth'

const firebaseConfig = {
  apiKey: 'AIzaSyB1zAgtEFCcFjFZ2zQ7AJL5ysvVDhpWRI0',
  authDomain: 'quasar--auth-81c7f.firebaseapp.com',
  projectId: 'quasar--auth-81c7f',
  storageBucket: 'quasar--auth-81c7f.appspot.com',
  messagingSenderId: '48107464766',
  appId: '1:48107464766:web:9a9c55bfa92c4cd83c896f'
}

initializeApp(firebaseConfig)

const auth = getAuth()

const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      unsubscribe()
      resolve(user)
    }, reject)
  })
}

export default { auth, getCurrentUser }
